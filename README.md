# 100DaysMLCode

100 Days of Machine Learning Coding as proposed by [Siraj Raval](https://github.com/llSourcell)

## Day 1(2018/10/01)| Siraj/Move37 : Markov Decision Processes/OpenAI Gym Installation

## Do:
 - Install `gym` library and run CartPole game simulation
 - Code solving CartPole game by methods: Random search, Hill climbing, Gradient Policy

### Practice Code
[CartPole](https://github.com/minhncedutw/siraj-move37/tree/master/markov-decision-processes/1_4_cartpole)

## Day 2(2018/10/02) : Unbalanced Data and solutions: Weighted categorical cross-entropy

### What is Unbalanced Data?

Imbalanced data typically refers to a problem with classification problems where the classes are not represented equally. 
In other words, the number of observations belonging to one class is significantly lower than those belonging to the other classes(maybe 10 times lower).

> You are working on your dataset. You create a classification model and get 90% accuracy immediately. “Fantastic” you think. You dive a little deeper and discover that 90% of the data belongs to one class. Damn!
> Most real-world classification problems display some level of class imbalance.
> However, most machine learning algorithms do not work very well with imbalanced datasets.

![](./images/day2-unbalanced-data.png)

### How to solve this problem?

Today I learnt and coded 2 solutions:

#### 1. Weight the categorical cross-entropy loss
Add parameters to weight the importance of each class. The minority but important classes are weighted high values(such as 5, 10, ...).
While the others may be weighted by 1 or smaller values.

![](./images/day2-weight-loss.png)

#### 2. Resample the training set-Over sampling
Duplicate values to increase the size of rare samples.
> Besides, we can do Under-sampling: reduce the size of the abundant class.

![](./images/day2-over-sampling.png)

### Practice Code
Unavailable because I coded for personal project.

### Reference
[Code for weighted categorical cross-entropy loss](https://gist.github.com/wassname/ce364fddfc8a025bfab4348cf5de852d#file-keras_weighted_categorical_crossentropy-py)

[Solution for Unbalanced Data-machinelearningmastery](https://machinelearningmastery.com/tactics-to-combat-imbalanced-classes-in-your-machine-learning-dataset/)

[Solution for Unbalanced Data-kdnuggetss](https://www.kdnuggets.com/2017/06/7-techniques-handle-imbalanced-data.html)

## Day 3(2018/10/03)| Siraj/Move37 : Google Dopamine-RL

### Main contents:

#### 1. The evolution of Deep RL network: DQN, C51, Rainbow, Implicit Quantile (Very New)
![](https://ai2-s2-public.s3.amazonaws.com/figures/2017-08-08/57f85af87e42f1a3d6e74d85a386073049f6586f/4-Figure1-1.png)
#### 2. Markov Decision process
![](https://image.slidesharecdn.com/ml-sep-09-091009141615-phpapp01/95/regretbased-reward-elicitation-for-markov-decision-processes-39-728.jpg?cb=1255098159)
#### 3. Bellman equation
![](https://images.slideplayer.com/16/4958328/slides/slide_18.jpg)

### Benmark methods:
![](./images/day3-benmark-dqn.png)

### Practice Code
[Code](https://github.com/minhncedutw/Google_Dopamine_LIVE/tree/master/MinhNC)

## Day4(2018/10/04) : AdamW(fastest optimizer)

### Useful Technology: AdamW (fastest way to train neural nets)

#### Algorithm explanation
[fastai](http://www.fast.ai/2018/07/02/adam-weight-decay/)

#### Source code
[Keras](https://github.com/GLambard/AdamW_Keras)

#### Benmark

Adam and another optimizers
![](http://www.fast.ai/images/adam_charts.png)

AdamW and Adam
![](http://www.fast.ai/images/adamw_charts.png)

#### Study Conclusion
AdamW is the best optimizer at the moment

#### Actual Experiment on personal project
AdamW is not able to converge. Was I wrong setting parameters?

#### Bonus
[Optimization for Deep Learning Highlights in 2017](http://ruder.io/deep-learning-optimization-2017/)
![](./images/day4-optimization.PNG)

### Useful knowledge
Where to find the source code of Papers: [http://www.gitxiv.com/](http://www.gitxiv.com/)

Tip for selecting specific GPUs:
```python
import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "2, 3, 4, 5"
```

[Tip for training Multi-GPU on Keras](https://datascience.stackexchange.com/questions/23895/multi-gpu-in-keras/25737#25737): 
```python
from keras.utils import multi_gpu_model
from keras import backend as K
print('Number gpus: ')
print(K.tensorflow_backend._get_available_gpus())
model = ...
try:
    model = multi_gpu_model(model)
except:
    pass
model.fit(...)
```

or
```python
import tensorflow as tf
import keras

config = tf.ConfigProto( device_count = {'GPU': 1 , 'CPU': 56} )
sess = tf.Session(config=config) 
keras.backend.set_session(sess)

model = ...

model.fit(x_train, y_train, epochs=epochs, validation_data=(x_test, y_test))
```

## Day5(2018/10/05) : LSTM

### Interesting Technology: Hybrid Network 3D-CNN & LSTM (best gesture prediction)

> Realtime Gesture Recognition using single RGB camera

#### Model Architecture: 
- An architecture that contains a three-dimensional convolutional network (3D-CNN) to extract spatiotemporal features
- A recurrent layer (LSTM) to model longer temporal relations
- And a softmax layer that outputs class probabilities.

![](https://cdn-images-1.medium.com/max/1000/0*jPRhu97XuT81gBrN.)

#### Source
[Article](https://medium.com/twentybn/gesture-recognition-using-end-to-end-learning-from-a-large-video-database-2ecbfb4659ff)

### LSTM recognize number plate

High level overview
![](https://cdn-images-1.medium.com/max/2000/1*sdb9_e5LVSJnxivblcFxEg.png)

Detailed theorem architecture
![](https://cdn-images-1.medium.com/max/2000/1*ppxHSM2dKhtH6lOTlKInfg.png)

![](https://cdn-images-1.medium.com/max/2000/1*JOmMMj4lxy4quHd4qjkd_g.png)

Actual architecture
![](https://cdn-images-1.medium.com/max/2000/1*MyHdDOccT2gqrCcNxKeQYA.png)

#### Source

[Article](https://hackernoon.com/latest-deep-learning-ocr-with-keras-and-supervisely-in-15-minutes-34aecd630ed8)

[Code](https://github.com/DeepSystems/supervisely-tutorials/blob/master/anpr_ocr/src/image_ocr.ipynb)

### Practice Code
[sample ConvLSTM2D to predict next scene of video](./codes/day5_train_test_convlstm2d.py)


### Useful Knowledge for LSTM problems

[synchronize the length of list of sequences: keras.sequence.pad_sequences](https://stackoverflow.com/questions/42943291/what-does-keras-io-preprocessing-sequence-pad-sequences-do)

## Day6(2018/10/06) : LSTM

### Theory
[Understanding-LSTMs](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

![](https://ai2-s2-public.s3.amazonaws.com/figures/2017-08-08/6ac8328113639044d2beb83246b9d07f513ac6c8/4-Figure4-1.png)

![](https://isaacchanghau.github.io/img/deeplearning/lstmgru/lstmandgru.png)

### Practice Code
[convert words to vectors then use conv1d and lstm to predict comment's attitude](./codes/day6_conv1dlstm_predict_comment_attitude.py)

### Knowledge
convert word to vector to feed to network
![](https://liip.rokka.io/www_inarticle/88d44e/embeddings.png)

## Day7(2018/10/07) : Residual Network

![](https://cdn-images-1.medium.com/max/987/1*pUyst_ciesOz_LUg0HocYg.png)

### Interesting network: PyramidNets (Deep Pyramidal Residual Networks)

[Pytorch source code](https://github.com/dyhan0920/PyramidNet-PyTorch)

![](https://user-images.githubusercontent.com/31481676/32331973-9d7dad2a-c027-11e7-9828-ac00fea0e5b5.png)

### Practice Code
[simple resnet predict cifar10](./codes/day7_resnet.py)

## Day8(2018/10/08) : Hierarchical RNN (LSTM)

### Practice code:
[mnist classification](./codes/day8_hierarchical_rnn.py)


## Day9(2018/10/09) : 

### Hot Machine Learning Topic

![](./images/day9-hot-ml-topics.PNG)

### Practice code:
[Optimize electric consumption by Monte Carlo reinforcement-learning](https://github.com/minhncedutw/Internet_of_Things_Optimization/blob/master/Practice.ipynb)


## Day10(2018/10/10) : Multilayred LSTM

### Practice code:
[mnist classification](./codes/day10_multilayred_hierarchical_rnn.py)

## Day11(2018/10/11) : Multilayred LSTM

### Practice code:
[mnist classification](./codes/day10_multilayred_hierarchical_rnn.py)

## Day 12(2018/10/15) : CapsuleNet

### Practice code:
[cifar10 classification](./codes/day12_capsulenet_classify_cifar10.py)

## Day 15(2018/10/15) : NASNet

## Day16(2018/10/16) : Progressive Neural Architecture Search

[Paper ArXiv](https://arxiv.org/abs/1712.00559)
[TF Code](https://github.com/chenxi116/PNASNet.TF)
[Pytorch Code](https://github.com/chenxi116/PNASNet.pytorch)

### Practice code:
[PyTorch implementation of PNASNet-5 on ImageNet](https://github.com/minhncsocial/PNASNet.pytorch)

### Interesting article: Graph convolutional neural network 
[A new graph convolutional neural network for web-scale recommender systems](https://medium.com/@Pinterest_Engineering/pinsage-a-new-graph-convolutional-neural-network-for-web-scale-recommender-systems-88795a107f48)

## Day17(2018/10/17) : Graph CNN

### Article Graph CNN
[Deep Learning on Knowledge Graphs with Keras](https://towardsdatascience.com/kegra-deep-learning-on-knowledge-graphs-with-keras-98e340488b93)
[Github source code](https://github.com/tkipf/keras-gcn)

> It is interesting that there are a lot of Graph Neural Networks like: GraphCNN, GraphConvLSTM, ...

>[Github source code](https://github.com/vermaMachineLearning/keras-deep-graph-learning)

### Interesting article:
[3D POINT CLOUD CLASSIFICATION USING DEEP LEARNING - September 20, 2017](http://www.itzikbs.com/3d-point-cloud-classification-using-deep-learning)


### Deep Learning for Object Detection from 2014 to now(2018):

[Source](https://github.com/hoya012/deep_learning_object_detection)
![](https://github.com/hoya012/deep_learning_object_detection/raw/master/assets/deep_learning_object_detection_history.PNG)

## Paper Survey Deep networks, Reinforcement Learning, Transfer Learning, Robot, ...:
[Paper Survey](https://github.com/shinshiner/Paper-Survey)

## PointCloud sources:

[https://github.com/yangyanli/PointCNN](https://github.com/yangyanli/PointCNN)

[https://github.com/maggie0106/Graph-CNN-in-3D-Point-Cloud-Classification](https://github.com/maggie0106/Graph-CNN-in-3D-Point-Cloud-Classification)

## Reinforcement-Learning source
[dennybritz/reinforcement-learning](https://github.com/dennybritz/reinforcement-learning/)



