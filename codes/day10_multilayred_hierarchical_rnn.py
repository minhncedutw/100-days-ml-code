'''
    File name: HANDBOOK
    Author: minhnc
    Date created(MM/DD/YYYY): 10/9/2018
    Last modified(MM/DD/YYYY HH:MM): 10/9/2018 9:46 AM
    Python Version: 3.6
    Other modules: [None]

    Copyright = Copyright (C) 2017 of NGUYEN CONG MINH
    Credits = [None] # people who reported bug fixes, made suggestions, etc. but did not actually write the code
    License = None
    Version = 0.9.0.1
    Maintainer = [None]
    Email = minhnc.edu.tw@gmail.com
    Status = Prototype # "Prototype", "Development", or "Production"
    Code Style: http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu/PythonGuidelines.html#module_formatting
'''

#==============================================================================
# Imported Modules
#==============================================================================
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os.path
import sys
import time

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "7"  # The GPU id to use, usually either "0" or "1"

import keras
from keras.datasets import mnist
from keras.models import Model
from keras.layers import Input, Dense, TimeDistributed, LSTM

#==============================================================================
# Constant Definitions
#==============================================================================
# Training parameters.
batch_size = 32
num_classes = 10
epochs = 5

# Embedding dimensions.
row_hidden = 128
col_hidden = 128

#==============================================================================
# Function Definitions
#==============================================================================


#==============================================================================
# Main function
#==============================================================================
def main(argv=None):
    print('Hello! This is XXXXXX Program')

    # The data, split between train and test sets.
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    print(x_train.shape)
    x_train = x_train.reshape(x_train.shape[0], 28, 28,)
    x_test = x_test.reshape(x_test.shape[0], 28, 28)
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    print('x_train: ', x_train.shape)

    y_train = keras.utils.to_categorical(y=y_train, num_classes=num_classes)
    y_test = keras.utils.to_categorical(y=y_test, num_classes=num_classes)

    input_shape = x_train.shape[1:]

    # Define the model
    inputs = Input(shape=input_shape)
    # encoded_rows = TimeDistributed(LSTM(row_hidden))(inputs)
    encoded_columns = LSTM(64, return_sequences=True)(inputs)
    encoded_columns = LSTM(col_hidden, return_sequences=True)(encoded_columns)
    encoded_columns = LSTM(col_hidden)(encoded_columns)
    outputs = Dense(num_classes, activation='softmax')(encoded_columns)
    model = Model(inputs=inputs, outputs=outputs)

    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    print(model.summary())

    # Train
    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test))

    # Evaluate
    scores = model.evaluate(x_test, y_test, verbose=1)
    print('Test loss: ', scores[0])
    print('Test accuracy: ', scores[1])




if __name__ == '__main__':
    main()
