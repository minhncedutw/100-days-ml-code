'''
    File name: use Conv1D-LSTM to predict comment's attitude
    Author: minhnc
    Date created(MM/DD/YYYY): 10/5/2018
    Last modified(MM/DD/YYYY HH:MM): 10/5/2018 3:27 PM
    Python Version: 3.6
    Other modules: [None]

    Copyright = Copyright (C) 2017 of NGUYEN CONG MINH
    Credits = [None] # people who reported bug fixes, made suggestions, etc. but did not actually write the code
    License = None
    Version = 0.9.0.1
    Maintainer = [None]
    Email = minhnc.edu.tw@gmail.com
    Status = Prototype # "Prototype", "Development", or "Production"
    Code Style: http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu/PythonGuidelines.html#module_formatting
'''

#==============================================================================
# Imported Modules
#==============================================================================
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os.path
import sys
import time

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "7"  # The GPU id to use, usually either "0" or "1"

from keras.datasets import imdb
from keras.preprocessing import sequence
from keras.models import Input, Model, Sequential
from keras.layers import Embedding, LSTM, Conv1D, MaxPool1D, Dense, Dropout, Activation

#==============================================================================
# Constant Definitions
#==============================================================================

#==============================================================================
# Function Definitions
#==============================================================================
def conv1dlstm_net(input_shape, max_features, embedding_size, input_length):
    input = Input(shape=input_shape, name='input')
    embed = Embedding(max_features, embedding_size, input_length=input_length)(input)
    drop = Dropout(0.25)(embed)
    conv1d = Conv1D(filters=64, kernel_size=5, strides=1, padding='valid', activation='relu')(drop)
    pool = MaxPool1D(pool_size=4)(conv1d)
    lstm = LSTM(units=70)(pool)
    output = Dense(units=1, activation='sigmoid')(lstm)
    model = Model(inputs=input, outputs=output)
    return model


def train_model(model, x_train, y_train, x_test, y_test, epochs, batch_size):
    train_history = model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(x_test, y_test))
    saving_name = 'conv1dlstm.model.h5'
    model.save(filepath=saving_name)
    return train_history, saving_name
#==============================================================================
# Main function
#==============================================================================
def main(argv=None):
    print('Hello! This is Conv1D-LSTM for Comment-Attitude-prediction program')

    '''
    Load data and preprocess data
    '''
    max_features = 20000
    maxlen = 100
    embedding_size = 128
    # Load data
    (x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=max_features)
    print('Num train sequences: ', len(x_train))
    print('Num test sequences: ', len(x_test))

    # Synchronize length of data
    x_train = sequence.pad_sequences(x_train, maxlen=maxlen)
    x_test = sequence.pad_sequences(x_test, maxlen=maxlen)
    print('x_train shape: ', x_train.shape)
    print('x_test shape: ', x_test.shape)
    input_shape = x_train.shape[1:]

    '''
    Define model
    '''
    model = conv1dlstm_net(input_shape=input_shape, max_features=max_features, embedding_size=embedding_size, input_length=maxlen)
    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])

    '''
    Train model
    '''
    train_history, saved_model = train_model(model=model, x_train=x_train, y_train=y_train, x_test=x_test, y_test=y_test, epochs=1, batch_size=32)

    '''
    Load model then Test & Evaluate model
    '''
    model.load_weights(filepath=saved_model)
    score, accuracy = model.evaluate(x_test, y_test, batch_size=32)
    print('Test score:', score)
    print('Test accuracy:', accuracy)


if __name__ == '__main__':
    main()
