'''
    File name: Use ConvLSTM2D to predict future scenes
    Author: minhnc
    Date created(MM/DD/YYYY): 10/5/2018
    Last modified(MM/DD/YYYY HH:MM): 10/5/2018 8:00 AM
    Python Version: 3.6
    Other modules: [None]

    Copyright = Copyright (C) 2017 of NGUYEN CONG MINH
    Credits = [None] # people who reported bug fixes, made suggestions, etc. but did not actually write the code
    License = None
    Version = 0.9.0.1
    Maintainer = [None]
    Email = minhnc.edu.tw@gmail.com
    Status = Prototype # "Prototype", "Development", or "Production"
    Code Style: http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu/PythonGuidelines.html#module_formatting
'''

#==============================================================================
# Imported Modules
#==============================================================================
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os.path
import sys
import time

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"  # The GPU id to use, usually either "0" or "1"

import numpy as np
import pylab as plt

from keras.models import Input, Model
from keras.layers import Conv3D, ConvLSTM2D
from keras.layers import BatchNormalization

#==============================================================================
# Constant Definitions
#==============================================================================

#==============================================================================
# Function Definitions
#==============================================================================
# Artificial data generation:
# Generate movies with 3 to 7 moving squares inside.
# The squares are of shape 1x1 or 2x2 pixels,
# which move linearly over time.
# For convenience we first create movies with bigger width and height (80x80)
# and at the end we select a 40x40 window.

def generate_movies(n_samples=1200, n_frames=15):
    row = 80
    col = 80
    noisy_movies = np.zeros((n_samples, n_frames, row, col, 1), dtype=np.float)
    shifted_movies = np.zeros((n_samples, n_frames, row, col, 1),
                              dtype=np.float)

    for i in range(n_samples):
        # Add 3 to 7 moving squares
        n = np.random.randint(3, 8)

        for j in range(n):
            # Initial position
            xstart = np.random.randint(20, 60)
            ystart = np.random.randint(20, 60)
            # Direction of motion
            directionx = np.random.randint(0, 3) - 1
            directiony = np.random.randint(0, 3) - 1

            # Size of the square
            w = np.random.randint(2, 4)

            for t in range(n_frames):
                x_shift = xstart + directionx * t
                y_shift = ystart + directiony * t
                noisy_movies[i, t, x_shift - w: x_shift + w,
                             y_shift - w: y_shift + w, 0] += 1

                # Make it more robust by adding noise.
                # The idea is that if during inference,
                # the value of the pixel is not exactly one,
                # we need to train the network to be robust and still
                # consider it as a pixel belonging to a square.
                if np.random.randint(0, 2):
                    noise_f = (-1)**np.random.randint(0, 2)
                    noisy_movies[i, t,
                                 x_shift - w - 1: x_shift + w + 1,
                                 y_shift - w - 1: y_shift + w + 1,
                                 0] += noise_f * 0.1

                # Shift the ground truth by 1
                x_shift = xstart + directionx * (t + 1)
                y_shift = ystart + directiony * (t + 1)
                shifted_movies[i, t, x_shift - w: x_shift + w,
                               y_shift - w: y_shift + w, 0] += 1

    # Cut to a 40x40 window
    noisy_movies = noisy_movies[::, ::, 20:60, 20:60, ::]
    shifted_movies = shifted_movies[::, ::, 20:60, 20:60, ::]
    noisy_movies[noisy_movies >= 1] = 1
    shifted_movies[shifted_movies >= 1] = 1
    return noisy_movies, shifted_movies

def convlstm_net(input_shape):
    input = Input(shape=input_shape, name='input')
    conv1 = ConvLSTM2D(filters=40, kernel_size=(3, 3), padding='same', return_sequences=True)(input)
    batch1 = BatchNormalization()(conv1)
    conv2 = ConvLSTM2D(filters=40, kernel_size=(3, 3), padding='same', return_sequences=True)(batch1)
    batch2 = BatchNormalization()(conv2)
    conv3 = ConvLSTM2D(filters=40, kernel_size=(3, 3), padding='same', return_sequences=True)(batch2)
    batch3 = BatchNormalization()(conv3)
    conv4 = ConvLSTM2D(filters=40, kernel_size=(3, 3), padding='same', return_sequences=True)(batch3)
    batch4 = BatchNormalization()(conv4)
    output = Conv3D(filters=1, kernel_size=(3, 3, 3), activation='sigmoid', padding='same', data_format='channels_last')(batch4)
    model = Model(inputs=input, outputs=output)
    return model


def train_net(model, x, y, epochs):
    model.compile(loss='binary_crossentropy', optimizer='adadelta')
    history = model.fit(x, y, batch_size=10, epochs=epochs, validation_split=0.05)
    saving_name = 'convlstm2d.model.h5'
    model.save(filepath=saving_name)
    return history, saving_name
#==============================================================================
# Main function
#==============================================================================
def main(argv=None):
    print('Hello! This is XXXXXX Program')

    '''
    Create training DATA
    '''
    n_samples = 1200 # number of training data
    noisy_movies, shifted_movies = generate_movies(n_samples=n_samples)
    print('Data shape: {} - {}'.format(noisy_movies.shape, shifted_movies.shape))
    input_shape = (None,) + noisy_movies.shape[2:]

    ## display training data
    import matplotlib.cm as cm
    for i in range(shifted_movies.shape[1]):
        plt.imshow(X=shifted_movies[0, i, :, :, 0], cmap=cm.gray)
        plt.show()

    '''
    Define then train deep network of convlstm
    '''
    model = convlstm_net(input_shape=input_shape)
    train_history, saved_model = train_net(model=model, x=noisy_movies[:1000], y=shifted_movies[:1000], epochs=1)

    '''
    Load model to deep network of convlstm
    '''
    ## load model
    model.load_weights(filepath=saved_model)

    ## select 1 sample
    which = 1004
    track = noisy_movies[which][:7, ::, ::, ::]

    ## predict
    for i in range(16):
        new_pos = model.predict(track[np.newaxis, ::, ::, ::, ::])
        new = new_pos[:, -1, :, :, :]
        track = np.concatenate((track, new), axis=0)

    ## show prediction
    track2 = noisy_movies[which][::, ::, ::, ::]
    for i in range(15):
        fig = plt.figure(figsize=(10, 5))

        ax = fig.add_subplot(121)

        if i >= 7:
            ax.text(1, 3, 'Predictions !', fontsize=20, color='w')
        else:
            ax.text(1, 3, 'Initial trajectory', fontsize=20)

        toplot = track[i, ::, ::, 0]

        plt.imshow(toplot)
        ax = fig.add_subplot(122)
        plt.text(1, 3, 'Ground truth', fontsize=20)

        toplot = track2[i, ::, ::, 0]
        if i >= 2:
            toplot = shifted_movies[which][i - 1, ::, ::, 0]

        plt.imshow(toplot)
        plt.savefig('%i_animate.png' % (i + 1))
    plt.show()

if __name__ == '__main__':
    main()
